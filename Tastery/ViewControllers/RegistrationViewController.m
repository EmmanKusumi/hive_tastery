//
//  RegistrationViewController.m
//  Tastery
//
//  Created by Emman Kusumi on 9/22/14.
//  Copyright (c) 2014 Kusumi. All rights reserved.
//

#import "RegistrationViewController.h"
#import "PasswordViewController.h"
#import "TasteryAPIManager.h"

@interface RegistrationViewController ()

@end

@implementation RegistrationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIBarButtonItem *confirm = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(onConfirm)];
    [[self navigationItem] setRightBarButtonItem:confirm];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onConfirm {
    // PasswordViewController *password = [PasswordViewController new];
    // [[self navigationController] pushViewController:password animated:YES];
    
    TasteryAPIManager *api = [TasteryAPIManager getTasteryAPIManager];
    [api registerWithFirstName:[txtFirstName text] lastName:[txtLastName text] email:[txtEmail text] mobile:[txtMobile text] telephone:[txtTelephone text] company:@"" address1:[txtAddress text] address2:@""city:[txtCity text] password:[txtPassword text] withCallback:^(NSDictionary *info) {
        
        NSString *message = [info valueForKey:@"message"];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
    }];
    
    [[self navigationController] popToRootViewControllerAnimated:YES];
}

@end
