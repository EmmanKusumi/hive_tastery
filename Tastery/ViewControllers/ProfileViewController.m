//
//  ProfileViewController.m
//  Tastery
//
//  Created by Emman Kusumi on 10/13/14.
//  Copyright (c) 2014 Kusumi. All rights reserved.
//

#import "ProfileViewController.h"
#import "TasterySocialManager.h"
#import "TasteryAppManager.h"
#import "TasteryAPIManager.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    TasteryAPIManager *api = [TasteryAPIManager getTasteryAPIManager];
    
    TasteryAppManager *tastery = [TasteryAppManager getTasteryAppManager];
    [[self navigationItem] setTitleView:[tastery getNavigationBarForTitle:@"Profile"]];

    TasterySocialManager *social = [TasterySocialManager getTasterySocialManager];
    TasteryAccount *account = [social currentAccount];
    
    [txtFirstName setText:[account firstname]];
    [txtLastName setText:[account lastname]];
    [txtEmail setText:[account email]];
    [txtTelephone setText:[account telephone]];
    [txtMobile setText:[account mobile]];
    [txtAddress setText:@""];
    [txtPassword setText:[api currentPassword]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
