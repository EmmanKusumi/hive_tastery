//
//  ModalMenuController.m
//  Tastery
//
//  Created by Max Von Ongkingco on 8/20/14.
//  Copyright (c) 2014 Kusumi. All rights reserved.
//

#import "ModalMenuController.h"
#import "TasteryAPIManager.h"
#import "TasteryAppManager.h"

@interface ModalMenuController ()

@end

@implementation ModalMenuController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    MenuObject *menu = [self menuObject];
    
    [lblName setText:[menu name]];
    [lblDescription setText:[menu description]];
    [lblPrice setText:[menu price]];
    
    if([menu thumb]) {
        if([[menu thumb] isKindOfClass:[NSString class]]) {
            NSString *encodedString = [menu thumb];
            encodedString = [encodedString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            
            NSURL *url = [NSURL URLWithString:encodedString];
            [self downloadImageWithURL:url completionBlock:^(BOOL succeeded, UIImage *image) {
                
                if(succeeded == YES) {
                    
                    [imgMenu setImage:image];
                    
                }
                
            }];
            
            // UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
            // [[cell imgMenu] setImage:image];
        }
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}

- (IBAction)onAddToCart:(id)sender {
    TasteryAPIManager *api = [TasteryAPIManager getTasteryAPIManager];
    [api addToCartForProductId:[[self menuObject] product_id] WithCallback:^(NSDictionary *info) {
        
        
        
    }];
}

@end
