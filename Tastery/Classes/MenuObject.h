//
//  MenuObject.h
//  Tastery
//
//  Created by Emman Kusumi on 9/30/14.
//  Copyright (c) 2014 Kusumi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuObject : NSObject

@property (nonatomic) NSString *day;
@property (nonatomic) NSString *description;
@property (nonatomic) NSString *href;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *price;
@property (nonatomic) NSString *product_id;
@property (nonatomic) NSString *rating;
@property (nonatomic) NSString *reviews;
@property (nonatomic) NSString *special;
@property (nonatomic) NSString *thumb;

- (void)readFromDictionary:(NSDictionary *) values;

@end
