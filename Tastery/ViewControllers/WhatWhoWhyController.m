//
//  WhatWhoWhyController.m
//  Tastery
//
//  Created by Max Von Ongkingco on 8/19/14.
//  Copyright (c) 2014 Kusumi. All rights reserved.
//

#import "WhatWhoWhyController.h"
#import "TasteryAPIManager.h"

@interface WhatWhoWhyController ()

@end

@implementation WhatWhoWhyController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    TasteryAPIManager *api = [TasteryAPIManager getTasteryAPIManager];
    
    
    if([self ViewType] == ViewType_Who) {
        [api getWhoWeAreWithCallback:^(PageObject *page) {
            [lblTitle setText:[page heading_title]];
            [lblDescription setText:[page description]];
        }];
    } else if([self ViewType] == ViewType_What) {
        [api getWhatWeDoWithCallback:^(PageObject *page) {
            [lblTitle setText:[page heading_title]];
            [lblDescription setText:[page description]];
        }];
    } else if([self ViewType] == ViewType_Why) {
        [api getWhyWeDoWithCallback:^(PageObject *page) {
            [lblTitle setText:[page heading_title]];
            [lblDescription setText:[page description]];
        }];
    } else if([self ViewType] == ViewType_How) {
        [api getHowWeDoWithCallback:^(PageObject *page) {
            [lblTitle setText:[page heading_title]];
            [lblDescription setText:[page description]];
        }];
    } else if([self ViewType] == ViewType_Gift) {
        [api getGiftWithCallback:^(PageObject *page) {
            [lblTitle setText:[page heading_title]];
            [lblDescription setText:[page description]];
        }];
    }
    
    // [lblTitle setText:[self Title]];
    // [lblDescription setText:[self Description]];
    
}

@end
