//
//  LoginController.m
//  Tastery
//
//  Created by Max Von Ongkingco on 8/19/14.
//  Copyright (c) 2014 Kusumi. All rights reserved.
//

#import "LoginController.h"
#import "TasteryAppManager.h"
#import "UserObject.h"
#import "TasterySocialManager.h"
#import "RegistrationViewController.h"
#import "TasteryAPIManager.h"

@interface LoginController ()

@end

@implementation LoginController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    TasteryAppManager *tastery = [TasteryAppManager getTasteryAppManager];
    [[self navigationItem] setTitleView:[tastery getNavigationBarForTitle:@"Login"]];
    
    for(UIView *view in [[self view] subviews]) {
        if([view isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)view;
            [[btn layer] setCornerRadius:25.0];
        }
    }
    
    // [btnFBLogin setDelegate:self];
    // [btnFBLogin setReadPermissions:@[@"public_profile", @"email", @"user_friends", @"read_stream",@"public_profile"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onFBLogin:(UIButton *)sender {
    for (id obj in [btnFBLogin subviews]) {
        if ([obj isKindOfClass:[UIButton class]]) {
            UIButton * loginButton =  obj;
            [loginButton sendActionsForControlEvents:UIControlEventTouchUpInside];
        }
        if ([obj isKindOfClass:[UILabel class]]){
            UILabel * loginLabel =  obj;
            loginLabel.text = @"";
            loginLabel.textAlignment = NSTextAlignmentCenter;
            [loginLabel setFont:[UIFont systemFontOfSize:27]];
            [loginLabel setHidden:YES];
        }
    }
}

#pragma mark Facebook SDK

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user {
    
    TasterySocialManager *social = [TasterySocialManager getTasterySocialManager];
    UserObject *userObject = [UserObject new];
    [userObject readFromDictionary:(NSDictionary *)user];
    [social setFBUser:userObject];
    
    // RegistrationViewController *registration = [RegistrationViewController new];
    // [[self navigationController] pushViewController:registration animated:YES];
    
}

- (IBAction)onLogin:(id)sender {
    TasteryAPIManager *api = [TasteryAPIManager getTasteryAPIManager];
    
    [api loginWithUsername:[txtEmail text]
                  Password:[txtPassword text]
              withCallback:^(TasteryAccount *account) {
        
            if([account approved] == YES) {
                TasterySocialManager *social = [TasterySocialManager getTasterySocialManager];
                [social setCurrentAccount:account];
            }
                  
        
    }];
    
    [[self navigationController] popToRootViewControllerAnimated:YES];
    
}

- (IBAction)onRegister:(id)sender {
    RegistrationViewController *registerView = [RegistrationViewController new];
    [[self navigationController] pushViewController:registerView animated:YES];
}

@end
