//
//  TasteryAPIManager.m
//  Tastery
//
//  Created by Emman Kusumi on 9/17/14.
//  Copyright (c) 2014 Kusumi. All rights reserved.
//

#import "TasteryAPIManager.h"

#define mainUrl @"http://tastery.ph/index.php?route="

#define whoWeArePath        @"product/category&path=60&json"
#define whatWeDoPath        @"product/category&path=61&json"
#define whyWeDoItPath       @"product/category&path=62&json"
#define howWeDoItPath       @"product/category&path=63&json"
#define giftPath            @"product/category&path=64_65&json"
#define loginPath           @"account/login&json"
#define registerPath        @"account/register&json"
#define menuPath            @"common/home&json"
#define addToCartPath       @"checkout/cart/add"
#define viewCartPath        @"checkout/cart&json"
#define checkoutPath        @"checkout/shipping_address/validate"
#define confirmCheckoutPath @"payment/cod/confirm"

@implementation TasteryAPIManager

static TasteryAPIManager *tasteryAPIManager = nil;

+ (TasteryAPIManager *) getTasteryAPIManager {
    if(tasteryAPIManager == nil) {
        tasteryAPIManager = [TasteryAPIManager new];
    }
    return tasteryAPIManager;
}

- (void)getWhoWeAreWithCallback:(void (^)(PageObject *page))callback {
    completionBlock = callback;
    [self makeGetRequestForPath:whoWeArePath];
}

- (void)getWhatWeDoWithCallback:(void (^)(PageObject *page))callback {
    completionBlock = callback;
    [self makeGetRequestForPath:whatWeDoPath];
}

- (void)getWhyWeDoWithCallback:(void (^)(PageObject *page))callback {
    completionBlock = callback;
    [self makeGetRequestForPath:whyWeDoItPath];
}

- (void)getHowWeDoWithCallback:(void (^)(PageObject *page))callback {
    completionBlock = callback;
    [self makeGetRequestForPath:howWeDoItPath];
}

- (void)getGiftWithCallback:(void (^)(PageObject *page))callback {
    completionBlock = callback;
    [self makeGetRequestForPath:giftPath];
}

- (void)getMenuWithCallback:(void (^)(NSMutableArray *menuList)) callback {
    completionBlock = callback;
    [self makeGetRequestForPath:menuPath];
}

- (void)getCartWithCallback:(void (^)(NSMutableArray *menuList)) callback {
    completionBlock = callback;
    [self makeGetRequestForPath:viewCartPath];
}

- (void)loginWithUsername:(NSString *) username
                 Password:(NSString *) password
             withCallback:(void (^)(TasteryAccount *account))callback {
    
    NSDictionary *login = [[NSDictionary alloc] initWithObjectsAndKeys:username,@"email",password,@"password",nil];
    [self setCurrentUsername:username];
    [self setCurrentPassword:password];
    
    completionBlock = callback;
    [self makePostRequestForPath:loginPath withValues:login];
}

- (void)registerWithFirstName:(NSString *) firstName
                     lastName:(NSString *) lastName
                        email:(NSString *) email
                       mobile:(NSString *) mobile
                    telephone:(NSString *) telephone
                      company:(NSString *) company
                     address1:(NSString *) address_1
                     address2:(NSString *) address_2
                         city:(NSString *) city
                     password:(NSString *) password withCallback:(void (^)(NSDictionary *info))callback {
    
//    firstname,lastname,email,mobile,telephone,company,address_1,address_2,city,password,confirm
//    Checkbox name: agree
    
    NSDictionary *signup = [[NSDictionary alloc] initWithObjectsAndKeys:firstName,@"firstname",lastName,@"lastname",email,@"email",mobile,@"mobile",telephone,@"telephone",company,@"company",address_1,@"address_1",address_2,@"address_2",city,@"city",password,@"password",@"1",@"confirm",@"1",@"agree",nil];
    
    completionBlock = callback;
    [self makePostRequestForPath:registerPath withValues:signup];
}

- (void)addToCartForProductId:(NSString *) productId
                 WithCallback:(void (^)(NSDictionary *info))callback {
    
    completionBlock = callback;
    
    NSDictionary *cart = [[NSDictionary alloc] initWithObjectsAndKeys:productId,@"product_id",nil];
    [self makePostRequestForPath:addToCartPath withValues:cart];
}

- (void)confirmCheckoutWithAddressId:(NSString *) addressId
                     shippingAddress:(NSString *) address
                        withCallback:(void (^)(NSDictionary *checkout))callback {
    
    completionBlock = callback;
    
    NSDictionary *checkOut = [[NSDictionary alloc] initWithObjectsAndKeys:address,@"shipping_address",addressId,@"address_id",nil];
    [self makePostRequestForPath:checkoutPath withValues:checkOut];
}

- (void)confirmCheckOut:(void (^)(NSMutableDictionary *info)) callback {
    completionBlock = callback;
    [self makeGetRequestForPath:confirmCheckoutPath];
}

/* ==============================
    REQUEST
 ===============================*/

- (void)makeGetRequestForPath:(NSString *) string {
    NSString *urlString = [NSString stringWithFormat:@"%@%@",mainUrl,string];
    
    NSLog(@"Start makeGetRequestForPath: %@",urlString);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:
                             [NSURL URLWithString:urlString]];
    [request setValue:string forHTTPHeaderField:@"RequestPath"];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request
                                                                delegate:self];
    
    [connection start];
}

- (void)makePostRequestForPath:(NSString *) string withValues:(NSDictionary *) value {
    NSString *urlString = [NSString stringWithFormat:@"%@%@",mainUrl,string];
    
    NSLog(@"Start makePostRequestForPath: %@",urlString);
    
    NSData *postData = [self encodeDictionary:value];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",mainUrl,string]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:[NSString stringWithFormat:@"%d", [postData length]] forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:string forHTTPHeaderField:@"RequestPath"];
    [request setHTTPBody:postData];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request
                                                                  delegate:self];
    [connection start];
}

#pragma mark NSURLDelegate

//- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response {
//    
//}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    NSError* error;
    
    NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSLog(@"didReceiveData Raw: %@", newStr);
    
    newStr = [newStr stringByReplacingOccurrencesOfString:@"<pre>" withString:@""];
    NSData* newData = [newStr dataUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:newData
                          options:kNilOptions
                          error:&error];
    
    NSLog(@"didReceiveData Json: %@", json);
    
    NSString *path = [[connection currentRequest] valueForHTTPHeaderField:@"RequestPath"];
    
    if([path rangeOfString:menuPath].length > 0) {
        NSMutableArray *menuList = [json valueForKey:@"products"];
        if(menuList != nil) {
            NSMutableArray *menuArray = [NSMutableArray new];
            
            for(NSDictionary *item in menuList) {
                MenuObject *menu = [MenuObject new];
                [menu readFromDictionary:item];
                [menuArray addObject:menu];
            }
            
            completionBlock(menuArray);
        }
    } else if([path rangeOfString:viewCartPath].length > 0) {
        NSMutableArray *carList = (NSMutableArray *)json;
        
        NSMutableArray *cartArray = [NSMutableArray new];
        for(NSDictionary *item in carList) {
            CartItem *cart = [CartItem new];
            [cart readFromDictionary:item];
            [cartArray addObject:cart];
        }
        
        completionBlock(cartArray);
        
    } else if([path rangeOfString:loginPath].length > 0) {
        
        NSMutableDictionary *account = [json valueForKey:@"account"];
        TasteryAccount *tasteryAccount = [TasteryAccount new];
        [tasteryAccount readFromDictionary:account];
        completionBlock(tasteryAccount);
        
    } else {
        NSMutableDictionary *pageInfo = [json valueForKey:@"page"];
        if(pageInfo != nil && [pageInfo count] > 0) {
            PageObject *page = [PageObject new];
            [page readFromDictionary:pageInfo];
            completionBlock(page);
        }
    }
}

- (NSData*)encodeDictionary:(NSDictionary*)dictionary {
    NSMutableArray *parts = [[NSMutableArray alloc] init];
    for (NSString *key in dictionary) {
        NSString *encodedValue = [[dictionary objectForKey:key] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodedKey = [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *part = [NSString stringWithFormat: @"%@=%@", encodedKey, encodedValue];
        [parts addObject:part];
    }
    NSLog(@"Post Data : %@",parts);
    NSString *encodedDictionary = [parts componentsJoinedByString:@"&"];
    return [encodedDictionary dataUsingEncoding:NSUTF8StringEncoding];
}

@end
