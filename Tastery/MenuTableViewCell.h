//
//  MenuTableViewCell.h
//  Tastery
//
//  Created by Emman Kusumi on 9/30/14.
//  Copyright (c) 2014 Kusumi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell

@property (nonatomic) IBOutlet UIImageView *imgMenu;

@end
