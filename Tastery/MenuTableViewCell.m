//
//  MenuTableViewCell.m
//  Tastery
//
//  Created by Emman Kusumi on 9/30/14.
//  Copyright (c) 2014 Kusumi. All rights reserved.
//

#import "MenuTableViewCell.h"

@implementation MenuTableViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
