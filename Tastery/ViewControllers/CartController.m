//
//  CartController.m
//  Tastery
//
//  Created by Emman Kusumi on 9/17/14.
//  Copyright (c) 2014 Kusumi. All rights reserved.
//

#import "CartController.h"
#import "CartTableCell.h"
#import "CartItem.h"
#import "TasteryAppManager.h"
#import "TasteryAPIManager.h"
#import "DeliveryInfoController.h"
#import "MenuObject.h"
#import "DateManager.h"
#import "TasterySocialManager.h"

@interface CartController ()

@end

@implementation CartController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    tastery = [TasteryAppManager getTasteryAppManager];
    [[self navigationItem] setTitleView:[tastery getNavigationBarForTitle:@"Cart"]];
    
    arrCartItems = [NSMutableArray new];
    
    TasteryAPIManager *api = [TasteryAPIManager getTasteryAPIManager];
    [api getCartWithCallback:^(NSMutableArray *menuList) {
        if([menuList count]>0) {
            arrCartItems = menuList;
            [tView reloadData];
        }
    }];
    
    [lblVat setText:@"119.40"];
    [lblTotal setText:@"995.00"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onShowDeliveryInfo:(id)sender {
    TasterySocialManager *social = [TasterySocialManager getTasterySocialManager];
    if([social currentAccount] == nil) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"You should login first before proceeding to checkout." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    } else {
        DeliveryInfoController *deliveryInfo = [DeliveryInfoController new];
        [[self navigationController] pushViewController:deliveryInfo animated:YES];
    }
}

#pragma mark UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrCartItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *strCellIdentifier = @"CellStop";
    
    CartTableCell *cell = (CartTableCell *)[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
    
    if(cell==nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CartTableCell" owner:self options:nil];
        cell = [topLevelObjects firstObject];
    }
    
    CartItem *cartItem = [arrCartItems objectAtIndex:[indexPath row]];
    
    NSString *date = [DateManager stringForDate:[cartItem odate]];
    
    [[cell lblDate] setText:date];
    [[cell lblItem] setText:[cartItem name]];
    [[cell txtQuantity] setText:[[cartItem quantity] stringValue]];
    [[cell txtDeliveryTime] setText:[cartItem dtime]];
    
    [[cell txtQuantity] setTag:0];
    [[cell txtQuantity] setDelegate:self];
    
    [[cell txtDeliveryTime] setTag:1];
    [[cell txtDeliveryTime] setDelegate:self];
    
    [[cell lblPrice] setText:[cartItem price]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 75;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [arrCartItems removeObjectAtIndex:[indexPath row]];
        [tView reloadData];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                             target:self
                                                                             action:@selector(onDone)];
    [[self navigationItem] setRightBarButtonItem:btnDone];
    
    if([textField tag]==0) {
        txtTime = nil;
        [textField setKeyboardType:UIKeyboardTypeNumberPad];
        return YES;
    } else if([textField tag]==1) {
        txtTime = textField;
        [datePicker addTarget:self action:@selector(onDateChange:) forControlEvents:UIControlEventValueChanged];
        textField.inputView = datePicker;
        return YES;
    }
    return NO;
}

- (void)onDone {
    
    [[self view] endEditing:YES];
    [[self navigationItem] setRightBarButtonItem:nil];
}

- (IBAction)onDateChange:(UIDatePicker *)sender {
    NSString *time = [DateManager stringForTime:[sender date]];
    [txtTime setText:time];
}

@end
