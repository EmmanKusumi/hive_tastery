//
//  TasteryAccount.m
//  Tastery
//
//  Created by Emman Kusumi on 10/13/14.
//  Copyright (c) 2014 Kusumi. All rights reserved.
//

#import "TasteryAccount.h"

@implementation TasteryAccount

- (void)readFromDictionary:(NSDictionary *) values {
    
    if([values respondsToSelector:@selector(allKeys)]) {
        for(id key in [values allKeys]) {
            @try {
                id value = [values valueForKey:key];
                if([key isEqualToString:@"approved"]) {
                    if([value isEqualToString:@"1"]) {
                        [self setApproved:YES];
                    }
                } else {
                    [self setValue:value forKey:key];
                }
            }
            @catch (NSException *exception) {
                NSLog(@"Undefined Key : %@", key);
            }
        }
    }
}

@end
