//
//  DeliveryInfoController.m
//  Tastery
//
//  Created by Emman Kusumi on 9/17/14.
//  Copyright (c) 2014 Kusumi. All rights reserved.
//

#import "DeliveryInfoController.h"
#import "TasteryAppManager.h"
#import "TasteryAPIManager.h"
#import "TasterySocialManager.h"

@interface DeliveryInfoController ()

@end

@implementation DeliveryInfoController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    TasteryAppManager *tastery = [TasteryAppManager getTasteryAppManager];
    [[self navigationItem] setTitleView:[tastery getNavigationBarForTitle:@"Delivery Info"]];
    
    TasteryAPIManager *api = [TasteryAPIManager getTasteryAPIManager];
    
    TasterySocialManager *social = [TasterySocialManager getTasterySocialManager];
    TasteryAccount *account = [social currentAccount];
    
    [txtFirstName setText:[account firstname]];
    [txtLastName setText:[account lastname]];
    [txtEmail setText:[account email]];
    [txtTelephone setText:[account telephone]];
    [txtMobile setText:[account mobile]];
    [txtAddress setText:@""];
    [txtPassword setText:[api currentPassword]];
    
    UIBarButtonItem *checkOut = [[UIBarButtonItem alloc] initWithTitle:@"Checkout" style:UIBarButtonItemStylePlain target:self action:@selector(onCheckout)];
    [[self navigationItem] setRightBarButtonItem:checkOut];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onCheckout {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Checkout"
                                                    message:@"Your order is being processed. Payment will be on delivery."
                                                   delegate:self
                                          cancelButtonTitle:@"Close"
                                          otherButtonTitles:nil];
    [alert show];
    
    TasterySocialManager *app = [TasterySocialManager getTasterySocialManager];
    TasteryAccount *account = [app currentAccount];
    
    TasteryAPIManager *api = [TasteryAPIManager getTasteryAPIManager];
    [api confirmCheckoutWithAddressId:[account address_id]
                      shippingAddress:[txtAddress text]
                         withCallback:^(NSDictionary *checkout) {
                             
                             
                             
        
    }];
    
    [api confirmCheckOut:^(NSMutableDictionary *info) {
       
        
        
    }];
    
}

#pragma mark UIAlertView

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [[self navigationController] popToRootViewControllerAnimated:YES];
}

@end
